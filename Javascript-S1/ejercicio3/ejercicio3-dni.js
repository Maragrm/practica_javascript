var letters = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var number = prompt("Introduce tu número de DNI (sin letra)");
var letter = prompt("Introduce la letra de tu DNI");
letter = letter.toUpperCase();

if(number < 0 || number > 99999999) {
  alert("El numero proporcionado no es valido");
}
else {
  var calcLetter = letters[number % 23];
  if(calcLetter != letter) {
    alert("DNI Incorrecto");
  }
  else {
    alert("DNI Válido");
  }
}


